FROM alpine:latest
ARG VER
RUN apk add --no-cache wget jq bash curl\
  && curl -sSL -o /usr/local/bin/kubectl  https://storage.googleapis.com/kubernetes-release/release/$VER/bin/linux/amd64/kubectl \
  && chmod +x /usr/local/bin/kubectl 
ENTRYPOINT ["/usr/local/bin/kubectl"]
CMD [""]
